import urllib
import urllib2
import json

import feedparser

import tornado
import tornado.ioloop

from tornado.httpserver import HTTPServer
from tornado.options import define, options
from tornado.web import Application
from tornado.web import RequestHandler
from tornado.web import url as tornado_url

PUBLIC_ENDPOINT = 'http://query.yahooapis.com/v1/public/yql'
SEARCH_ENDPOINT = 'http://autoc.finance.yahoo.com/autoc'
HEADLINES_EMDPOINT = 'http://finance.yahoo.com/rss/headline'


def get_data(url, params):
    params = urllib.urlencode(params)
    url = '%s?%s' % (url, params)

    print url

    return json.loads(urllib2.urlopen(url).read())


class Info(RequestHandler):
    def get(self, symbol):
        params = {
            'symbol': symbol,
            'q': 'select * from \
                yahoo.finance.quotes where symbol=@symbol',
            'env': 'store://datatables.org/alltableswithkeys',
            'format': 'json'
        }


        self.finish(get_data(
            PUBLIC_ENDPOINT, params).get('query').get('results').get('quote')
        )


class Headlines(RequestHandler):
    def get(self, symbol):
        params = {
            's': symbol,
        }
        params = urllib.urlencode(params)
        url = '%s?%s' % (HEADLINES_EMDPOINT, params)

        headlines = []

        print url

        data = feedparser.parse(urllib.urlopen(url))['entries']
        for x in data:
            headlines.append({'id': x.id,
                'title': x.title,
                'link': x.link,
                }
            )
        self.finish({'data': headlines})


class HeadlineView(RequestHandler):
    def get(self, id):
        params = {
            'id': id,
        }
        params = urllib.urlencode(params)
        url = '%s?%s' % (HEADLINES_EMDPOINT, params)
        headlines = []
        data = feedparser.parse(urllib.urlopen(url))['entries']
        for x in data:
            headlines.append({'id': x.id, 'title': x.title})
        self.finish({'data': headlines})


class History(RequestHandler):
    def get(self, symbol, start, end):

        params = {
            'symbol': symbol,
            'start': start,
            'end': end,
            'q': 'select * from yahoo.finance.historicaldata \
                where symbol=@symbol and startDate=@start and endDate=@end',
            'env': 'store://datatables.org/alltableswithkeys',
            'format': 'json'
        }

        self.finish(get_data(PUBLIC_ENDPOINT, params))


class Search(RequestHandler):
    def get(self, symbol):

        params = {
            'query': symbol,
            'callback': 'YAHOO.Finance.SymbolSuggest.ssCallback'
        }

        url = SEARCH_ENDPOINT

        params = urllib.urlencode(params)
        url = '%s?%s' % (url, params)
        d = urllib2.urlopen(url).read()
        print d
        return


def main():
    handlers = (
        tornado_url('/(.*)/info', Info),
        tornado_url('/(.*)/headlines', Headlines),
        tornado_url('/(.*)/search', Search),
        tornado_url('/(.*)/(.*)/(.*)/history', History),
    )

    settings = dict(debug=True, xsrf_cookies=False)

    define('port', type=int, default=8888)
    define('host', type=str, default='127.0.0.1')

    application = Application(
        handlers=handlers, **settings
    )

    tornado.options.parse_command_line()
    http_server = HTTPServer(application, xheaders=True)
    http_server.listen(options.port, options.host)

    try:
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        tornado.ioloop.IOLoop.instance().stop()

if __name__ == '__main__':
    main()
